export PATH=/usr/local/Cellar/ruby/1.9.3-p194/bin:$PATH
export PATH=/usr/local/bin:$PATH
export PATH=/usr/local/bin:$PATH
code () {
    if [[ $# = 0 ]]
    then
       open -a "Visual Studio Code"
    else
        [[ $1 = /* ]] && F="$1" || F="$PWD/${1#./}"
       open -a "Visual Studio Code" "$F"
    fi
}

# added by Anaconda2 2.5.0 installer
export PATH="//anaconda/bin:$PATH"

# added by Anaconda2 2.5.0 installer
export PATH="/Users/apple/anaconda2/bin:$PATH"
